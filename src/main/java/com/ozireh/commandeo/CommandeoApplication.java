package com.ozireh.commandeo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommandeoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommandeoApplication.class, args);
	}

}
