package com.ozireh.commandeo.entities;

import java.util.Optional;

public class Item {
	private Optional<Product> product;
	private int quantity;
	
	
	/**
	 * @return the product
	 */
	public Optional<Product> getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Optional<Product> product) {
		this.product = product;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public Item(Optional<Product> product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}
	public Item() {
	}
	
}
