package com.ozireh.commandeo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class SecurityController {
	
	@GetMapping("/login")
	public String login(String name, Model model) {
		model.addAttribute("title","login");
		
		return "login";
	}
}
