package com.ozireh.commandeo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ozireh.commandeo.services.ProductServiceImpl;

@Controller
@RequestMapping("/product") 

public class ProductController {
	@Autowired
	ProductServiceImpl productService;
	
	@GetMapping("/list")
	public String indexProduct(String name, Model model) {
		model.addAttribute("title", "List of products");
		model.addAttribute("products",this.productService.findAll());
		
		return "product/indexProduct";
	}
	
	@GetMapping("/add")
	public String addProduct(String name, Model model) {
		model.addAttribute("Title", "Add product");
		return "product/addProduct";
	}
}
