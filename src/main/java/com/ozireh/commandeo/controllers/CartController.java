package com.ozireh.commandeo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ozireh.commandeo.services.CartServiceImpl;
import com.ozireh.commandeo.services.ProductServiceImpl;

@Controller
@RequestMapping("/cart")
public class CartController {
	@Autowired
	private CartServiceImpl cartService;
	@Autowired
	private ProductServiceImpl productService;
	
	@GetMapping("/items")
	public String index(Model model) {
		model.addAttribute("products", cartService.getCart());
		return "cart/index";
	}
	
	@RequestMapping(value="/buy/{id}", method=RequestMethod.GET)
	public String buy(@PathVariable("id") int id,ModelMap model) {
		productService.findById(id).ifPresent(product -> cartService.addProduct(product));
		return "redirect:/cart/items";
	}
	
	@RequestMapping(value="/edit/{id}/{qt}", method=RequestMethod.GET)
	public String editQuantity(@PathVariable("id") int id, @PathVariable("qt") int qt, ModelMap model) {
		productService.findById(id).ifPresent(product -> cartService.editQuantity(product, qt));
		return "redirect:/cart/items";
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public String delete(@PathVariable("id") int id, ModelMap model) {
		productService.findById(id).ifPresent(product -> cartService.deleteProduct(product));
		return "redirect:/cart/items";
	}
}
