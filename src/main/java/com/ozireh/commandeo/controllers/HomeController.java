package com.ozireh.commandeo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.ozireh.commandeo.services.ProductServiceImpl;

@Controller
public class HomeController {
	@Autowired
	ProductServiceImpl productService;
	
	@GetMapping("/")
	public String homePage(String name, Model model) {
		model.addAttribute("name", "Home");
		model.addAttribute("products", productService.findAll());
		
		return "home";
	}
}
