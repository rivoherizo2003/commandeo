package com.ozireh.commandeo.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ozireh.commandeo.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>{
	@Query(value="SELECT * FROM product where 1=1", nativeQuery=true)
	Collection<Product> findAllActiveProduct();
}
