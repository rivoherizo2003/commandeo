package com.ozireh.commandeo.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ozireh.commandeo.entities.Product;
import com.ozireh.commandeo.repositories.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	ProductRepository productRepository;
	
	public Iterable<Product> findAll(){
		return this.productRepository.findAll();
	}
	
	public Optional<Product> findById(int id) {
		return this.productRepository.findById(id);
	}
	
	public Iterable<Product> findAllActive(){
		return this.productRepository.findAllActiveProduct();
	}
}
