package com.ozireh.commandeo.services;

import java.util.Map;

import com.ozireh.commandeo.entities.Product;

public interface CartService {
	public Map<Product, Integer> getCart();
}
