package com.ozireh.commandeo.services;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.ozireh.commandeo.entities.Product;

@Service
@Scope(value=WebApplicationContext.SCOPE_SESSION, proxyMode=ScopedProxyMode.TARGET_CLASS)
@Transactional
public class CartServiceImpl implements CartService{
	private Map<Product, Integer> cart = new HashMap<Product, Integer>();
	
	/**
	 * add new product to the cart
	 * @param product
	 */
	public void addProduct(Product product){
		if (cart.containsKey(product)) {
			cart.replace(product, cart.get(product) + 1);
		} else {
			cart.put(product, 1);
		}
	}
	
	/**
	 * edit quantity
	 * @param product
	 * @param qty
	 */
	public void editQuantity(Product product, int qty){
		if (cart.containsKey(product)) {
			cart.replace(product, qty);
		}
	}

	@Override
	public Map<Product, Integer> getCart() {
		// TODO Auto-generated method stub
		return Collections.unmodifiableMap(cart);
	}	
	
	/**
	 * 
	 * @param product
	 */
	public void deleteProduct(Product product) {
		if (cart.containsKey(product)) {
			cart.remove(product);
		}
	}
}
