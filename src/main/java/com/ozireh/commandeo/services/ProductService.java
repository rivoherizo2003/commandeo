package com.ozireh.commandeo.services;

import java.util.Optional;

import com.ozireh.commandeo.entities.Product;

public interface ProductService {
	public Optional<Product> findById(int id);
	public Iterable<Product> findAll();
}
